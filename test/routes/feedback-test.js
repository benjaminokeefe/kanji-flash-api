const app = require('../../app');
const request = require('supertest');
const mockFeedback = {
    data: {
        attributes: {
            text: 'This is some feedback!'
        }
    }
};

describe('ROUTE - Feedback', function () {
    it('returns a 201 response for a well-formed request', function (done) {
        request(app)
            .post('/feedback')
            .set('Content-Type', 'application/vnd.api+json')
            .send(mockFeedback)
            .expect(201, done);
    });

    it('returns a 500 response for an incorrectly-formed request', function (done) {
        request(app)
            .post('/feedback')
            .set('Content-Type', 'application/vnd.api+json')
            .send({})
            .expect(500, done);
    });
});