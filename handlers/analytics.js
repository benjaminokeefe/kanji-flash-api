'use strict';

const
  mongoose = require('mongoose'),
  Analytic = require('../models/analytic');

module.exports = {
  insertAnalytic (analytic) {
    return new Promise((resolve, reject) => {
      let newAnalytic = new Analytic(analytic);

      newAnalytic.save((err, analytic) => {
        if (err) reject(err);
        resolve(analytic);
      });
    });
  },

  getAnalytics (query = {}) {
    return new Promise((resolve, reject) => {
      Analytic.find(query, (err, analytics) => {
        if (err) reject(err);
        resolve(analytics);
      });
    });
  }
};
