'use strict';

const 
  mongoose = require('mongoose'),
  Kanji = require('../models/kanji');

module.exports = {
  getKanji: (query) => {
    const 
      sanitizedQuery = getSanitizedQuery(query),
      sortOptions = getSortOptions(query);

    return Kanji.find(sanitizedQuery).sort(sortOptions).exec();
  },

  getKanjiById: (id) => {
    return Kanji.findOne({ _id: id }).exec();
  }
}

function getSanitizedQuery (query) {
  if (query.jlpt) {
    return { "jlpt.level": query.jlpt.replace(/n/i, '') }
  }

  return query;
}

function getSortOptions (query) {
  if (query.jlpt) {
    return { "jlpt.level": -1, "jlpt.order": 1 };
  }

  return {};
}