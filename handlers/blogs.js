'use strict';

const
  mongoose = require('mongoose'),
  Blog = require('../models/blog');

module.exports = {
  getBlogs: () => {
    return new Promise ((resolve, reject) => {
      Blog.find({}, (err, blogs) => {
        if (err) {
          reject(err);
        } else {
          resolve(blogs);
        }
      });
    });
  },

  getBlogById: id => {
    return new Promise ((resolve, reject) => {
      Blog.findOne({ _id: id}, (err, blog) => {
        if (err) {
          reject(err);
        } else {
          resolve(blog);
        }
      });
    });
  },

  getBlogsByQuery: query => {
    return new Promise ((resolve, reject) => {
      Blog.find(query, (err, blog) => err ? reject(err) : resolve(blog));
    });
  },

  insertBlog: blog => {
    return new Promise ((resolve, reject) => {
      let newBlog = new Blog({
        dateCreated: blog.dateCreated,
        title: blog.title,
        author: blog.author,
        entry: blog.entry
      });

      newBlog.save(function (err, blog) {
        if (err) {
          reject(err);
        } else {
          resolve(blog);
        }
      });
    });
  },

  deleteBlog: id => {
    return new Promise((resolve, reject) => {
      Blog.findOneAndRemove({ _id: id }, function (err, blog) {
        if (err) {
          reject(err);
        } else {
          resolve(blog);
        }
      });
    });
  }
}
