'use strict';

const mongoose = require('mongoose');
const Feedback = require('../models/feedback');

module.exports = {
  insertFeedback (feedback) {
    return new Promise ((resolve, reject) => {
      let newFeedback = new Feedback({
          type: 'feedback',
          text: feedback.text,
          dateCreated: new Date(),
          userId: feedback.userId
      });

      newFeedback.save(function (err) {
        if (err) reject(err);
        resolve();
      });
    });
  },

  getFeedback (query = {}) {
    return new Promise((resolve, reject) => {
      Feedback.find(query, (err, feedback) => {
        if (err) reject(err);
        resolve(feedback);
      });
    });
  }
}
