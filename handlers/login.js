'use strict';
const
  jwt = require('jsonwebtoken'),
  bcrypt = require('bcrypt'),
  User = require('../models/user'),
  format = require('../utils/json-api-formatter'),
  tokenUtil = require('../utils/token');

module.exports = {
  authenticate: (email, password) => {
    return new Promise((resolve, reject) => {
      if (!email || !password) reject({ message: 'Invalid username or password', status: 401 });

      User.findOne({ email: email }, (err, user) => {
        if (err) {
          reject({ message: 'Invalid username or password', status: 403 });
        } else if (user && user.password) {
          bcrypt.compare(password, user.password, (err, res) => {
            if (err || !res) reject({ message: 'Invalid username or password', status: 403 });

            const token = tokenUtil.generateToken(user);
            if (token) {
              let mappedUser = format.user(user);
              mappedUser.data.attributes.token = token;
              resolve(mappedUser);
            } else {
              reject({ message: 'Invalid token', status: 403 });
            }
          });
        } else {
          reject({ message: 'Invalid username or password', status: 401 });
        }
      });
    });
  },

  validate: token => {
    return new Promise((resolve, reject) => {
      if (token) {
        jwt.verify(token, process.env.SECRET_ACCESS_KEY, function (err, decoded) {
          if (err) reject({ message: 'Failed to authenticate token', status: 403 });
          resolve(decoded);
        });
      } else {
        reject({ message: 'No token provided', status: 403 });
      }
    });
  }
}
