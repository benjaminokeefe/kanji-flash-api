'use strict';

const
  mongoose = require('mongoose'),
  User = require('../models/user');

module.exports = {
  getUsers: (query) => {
    return new Promise ((resolve, reject) => {
      User.find(query, (err, users) => {
        if (err) {
          reject(err);
        } else {
          resolve(users);
        }
      });
    });
  },

  getUserById: id => {
    return new Promise ((resolve, reject) => {
      User.findOne({ _id: id }, (err, user) => {
        if (err) {
          reject(err);
        } else {
          resolve(user);
        }
      });
    });
  },
  
  insertUser: user => {
    return new Promise ((resolve, reject) => {
      let newUser = new User({
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        password: user.password,
        accountType: user.accountType,
        isAdmin: user.isAdmin,
        dateCreated: user.dateCreated,
        preferences: {
          receiveNewsletter: user.preferences.receiveNewsletter,
        }
      });

      newUser.save(function (err, user) {
        if (err) {
          reject(err);
        } else {
          resolve(user);
        }
      });
    });
  },

  deleteUser: id => {
    return new Promise((resolve, reject) => {
      User.findOneAndRemove({ _id: id }, function (err, user) {
        if (err) {
          reject(err);
        } else {
          resolve(user);
        }
      });
    });
  }
}
