'use strict';

const mongoose = require('mongoose');
const Deck = require('../models/deck');
const format = require('../utils/json-api-formatter');

module.exports = {
  getDeckById: id => {
    return new Promise((resolve, reject) => {
      Deck
        .findOne({ _id: id })
        .populate('kanji')
        .exec((err, docs) => {
          if (err) reject(err);
          else {
            const deck = docs.toObject();
            deck.kanji = format.kanji(deck.kanji).data;
            resolve(deck);
          }
        });
    });
  },

  insertDeck: deck => {
    return new Promise ((resolve, reject) => {
      let newDeck = new Deck({
        name: deck.name,
        dateCreated: deck.dateCreated,
        ownerId: deck.ownerId,
        sharedUserIds: deck.sharedUserIds,
        kanji: deck.kanji
      });

      newDeck.save(function (err, deck) {
        if (err) reject(err)
        else resolve(deck);
      });
    });
  },

  deleteDeck: id => {
    return new Promise((resolve, reject) => {
      Deck.findOneAndRemove({ _id: id }, function (err, deck) {
        if (err) reject(err);
        else resolve(deck);
      });
    });
  },

  updateDeckProperty: (id, property) => {
    return new Promise((resolve, reject) => {
      Deck.findOneAndUpdate({ _id: id }, property, { new: true }, function (err, deck) {
        if (err) reject(err);
        else resolve(deck);
      });
    });
  },

  addCard: (id, kanjiId) => {
    return new Promise((resolve, reject) => {
      Deck.findByIdAndUpdate(id, { $push: { "kanji": kanjiId }}, { new: true }, function (err, deck) {
        if (err) reject(err);
        else resolve(deck);
      });
    });
  },

  removeCard: (id, kanjiId) => {
    return new Promise((resolve, reject) => {
      Deck.update({ _id: id }, { $pullAll: { kanji: [kanjiId] } }, { new: true }, function (err, deck) {
        if (err) reject(err);
        else resolve(deck);
      });
    });
  },

  getDeckMeta: (ownerId) => {
    return new Promise((resolve, reject) => {
      const query = ownerId ? { ownerId } : {};
      Deck.find(query, 'name', function (err, meta) {
        if (err) {
          reject(err);
        } else {
          const mappedMeta = meta.map(m => ({ id: m._id, name: m.name }));
          resolve(mappedMeta);
        }
      });
    });
  }
};
