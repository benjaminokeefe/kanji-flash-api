'use strict';
const
  express = require('express'),
  app = express(),
  cors = require('cors'),
  morgan = require('morgan'),
  blogs = require('./routes/blogs'),
  users = require('./routes/users'),
  kanji = require('./routes/kanji'),
  login = require('./routes/login'),
  analytics = require('./routes/analytics'),
  feedback = require('./routes/feedback'),
  decks = require('./routes/decks'),
  mongoose = require('mongoose'),
  path = require('path'),
  favicon = require('serve-favicon'),
  bodyParser = require('body-parser');

if (app.get('env') === 'development') {
  // Load the environment variables for development
  require('dotenv').config();
}

mongoose.connect(process.env.DATABASE_URL);

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.json({ type: 'application/json' }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/blogs', blogs);
app.use('/users', users);
app.use('/kanji', kanji);
app.use('/login', login);
app.use('/analytics', analytics);
app.use('/feedback', feedback);
app.use('/decks', decks);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500).send(err.message);
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500).send(err.message);
});

module.exports = app;
