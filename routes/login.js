'use strict';

const
  express = require('express'),
  router = express.Router(),
  handler = require('../handlers/login');

router.post('/', (req, res, next) => {
  const
    email = req.body.email,
    password = req.body.password;

  if (!email || !password) res.status(401).send('Invalid email or password');

  handler.authenticate(email, password)
  .then(function (user) {
    res.status(200).send(user);
  })
  .catch(function (err) {
    res.status(err.status).send(err.message);
  });
});

router.post('/validate', (req, res, next) => {
  const token = req.body.token;
  handler.validate(token)
    .then(decoded => res.send(decoded))
    .catch(err => res.status(err.status).send(err.message));
});

module.exports = router;
