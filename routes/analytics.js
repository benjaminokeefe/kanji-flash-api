'use strict';

const
  express = require('express'),
  router = express.Router(),
  handler = require('../handlers/analytics'),
  loginHandler = require('../handlers/login');

router.get('/', (req, res, next) => {
  loginHandler.validate(req.headers['x-access-token'])
  .then(decoded => {
    if (decoded && decoded.isAdmin) {
      handler.getAnalytics(req.query)
        .then(analytics => res.send(analytics))
        .catch(err => res.send(err));
    } else {
      res.status(403).send('Forbidden');
    }
  })
  .catch(err => res.send(err));
});

router.post('/', (req, res, next) => {
  handler.insertAnalytic(req.body).then(analytic => {
    res.status(200).send(analytic);
  }).catch(err => {
    res.status(err.status).send(err.message);
  })
});

module.exports = router;
