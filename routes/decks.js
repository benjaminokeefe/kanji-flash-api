'use strict';

const express = require('express');
const handler = require('../handlers/decks');
const router = express.Router();
const loginHandler = require('../handlers/login');

router.get('/meta', (req, res, next) => {
  handler.getDeckMeta(req.query.ownerId)
    .then(meta => res.json(meta))
    .catch(err => res.send(err));
});

router.get('/:id', (req, res, next) => {
  handler.getDeckById(req.params.id)
    .then(deck => res.json(deck))
    .catch(err => res.send(err));
});

router.post('/', (req, res, next) => {
  loginHandler.validate(req.headers['x-access-token'])
  .then(decoded => {
    if (decoded) {
      handler.insertDeck({
        name: req.body.name,
        dateCreated: req.body.dateCreated,
        ownerId: req.body.ownerId,
        sharedUserIds: [],
        kanji: req.body.kanji
      })
      .then (deck => res.json(deck))
      .catch(err => res.send(err));
    } else {
      res.status(403).send('Unauthorized');
    }
  })
  .catch(err => {
    res.status(err.status).send(err.message);
  });
});

router.delete('/:id', (req, res, next) => {
  const deckId = req.params.id;
  loginHandler.validate(req.headers['x-access-token'])
  .then(decoded => {
    if (decoded) {
      handler.getDeckById(req.params.id)
      .then((deck) => {
        if ((deck.ownerId.toString() === decoded._id) || decoded.isAdmin) {
          handler.deleteDeck(deckId)
            .then(deck => res.json(deck))
            .catch(err => res.send(err));
        } else {
          res.status(403).send('Unauthorized');
        }
      })
      .catch(err => res.status(500).send(`Deck ID: ${deckId} could not be deleted`));
    } else {
      res.status(403).send('Unauthorized');
    }
  })
  .catch(err => {
    res.status(err.status).send(err.message);
  });
});

router.patch('/:id', (req, res, next) => {
  const deckId = req.params.id;
  const property = req.body;
  loginHandler.validate(req.headers['x-access-token'])
  .then(decoded => {
    if (decoded) {
      handler.getDeckById(req.params.id)
      .then((deck) => {
        if ((deck.ownerId.toString() === decoded._id) || decoded.isAdmin) {
          handler.updateDeckProperty(deckId, property)
            .then(deck => res.json(deck))
            .catch(err => res.send(err));
        } else {
          res.status(403).send('Unauthorized');
        }
      })
      .catch(err => res.status(500).send(`Deck ID: ${deckId} could not be updated`));
    } else {
      res.status(403).send('Unauthorized');
    }
  });
});

router.patch('/:id/kanji/:kanjiId', (req, res, next) => {
  const deckId = req.params.id;
  const kanjiId = req.params.kanjiId;
  loginHandler.validate(req.headers['x-access-token'])
  .then(decoded => {
    if (decoded) {
      handler.getDeckById(req.params.id)
      .then((deck) => {
        if (deck.kanji.map(k => k.id.toString()).includes(kanjiId)) {
          res.send('This kanji already exists in this deck');
        } else {
          if ((deck.ownerId.toString() === decoded._id) || decoded.isAdmin) {
            handler.addCard(deckId, kanjiId)
              .then(deck => res.json(deck))
              .catch(err => res.send(err));
          } else {
            res.status(403).send('Unauthorized');
          }
        }
      })
      .catch(err => res.status(500).send(`Deck ID: ${deckId} could not add Card ID: ${cardId}`));
    } else {
      res.status(403).send('Unauthorized');
    }
  });
});

router.delete('/:id/kanji/:kanjiId', (req, res, next) => {
  const deckId = req.params.id;
  const kanjiId = req.params.kanjiId;
  loginHandler.validate(req.headers['x-access-token'])
  .then(decoded => {
    if (decoded) {
      handler.getDeckById(req.params.id)
      .then((deck) => {
        if (!deck.kanji.map(k => k.id.toString()).includes(kanjiId)) {
          res.send('This kanji does not exist in this deck');
        } else {
          if ((deck.ownerId.toString() === decoded._id) || decoded.isAdmin) {
            handler.removeCard(deckId, kanjiId)
              .then(deck => res.json(deck))
              .catch(err => res.send(err));
          } else {
            res.status(403).send('Unauthorized');
          }
        }
      })
      .catch(err => res.status(500).send(`Deck ID: ${deckId} could not remove Card ID: ${cardId}`));
    } else {
      res.status(403).send('Unauthorized');
    }
  });
});

module.exports = router;
