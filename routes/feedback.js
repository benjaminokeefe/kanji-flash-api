'use strict';

const express = require('express');
const router = express.Router();
const loginHandler = require('../handlers/login');
const handler = require('../handlers/feedback');

router.get('/', (req, res, next) => {
  loginHandler.validate(req.headers['x-access-token'])
  .then(decoded => {
    if (decoded && decoded.isAdmin) {
      handler.getFeedback(req.query)
        .then(feedback => res.send(feedback))
        .catch(err => res.send(err));
    } else {
      res.status(403).send('Forbidden');
    }
  })
  .catch(err => res.send(err));
});

router.post('/', (req, res, next) => {
  handler.insertFeedback(req.body)
    .then(() => res.sendStatus(201))
    .catch(err => res.send(err));
});

module.exports = router;
