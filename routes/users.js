'use strict';

const
  express = require('express'),
  router = express.Router(),
  bcrypt = require('bcrypt'),
  format = require('../utils/json-api-formatter'),
  loginHandler = require('../handlers/login'),
  handler = require('../handlers/users'),
  tokenUtil = require('../utils/token'),
  BCRYPT_SALT_ROUNDS = 10;
  
router.get('/', (req, res, next) => {
  loginHandler.validate(req.headers['x-access-token'])
  .then(decoded => {
    if (decoded && decoded.isAdmin) {
      handler.getUsers({})
      .then(users => {
        res.json(format.user(users));
      })
      .catch(err => {
        res.send(err);
      });
    } else {
      res.status(403).send('Unauthorized');
    }
  })
  .catch(err => {
    res.status(err.status).send(err.message);
  });
});

router.get('/validate/:value', (req, res, next) => {
  const 
    property = req.query.property,
    value = req.params.value,
    query = {};
  
  query[property] = value;  
  
  if (!property || !value) res.status(400).send('Error: must include value in path and property in query');
  
  handler.getUsers(query)
  .then(data => {
    // If a value exists, the property isn't unique
    data.length ? res.send(true) : res.send(false);
  })
  .catch(err => {
    res.send(err.status).send(err.message);
  })
});

router.get('/:id', (req, res, next) => {
  loginHandler.validate(req.headers['x-access-token'])
  .then(decoded => {
    if (decoded && (decoded.isAdmin || decoded._id === req.params.id)) {
      handler.getUserById(req.params.id)
      .then(user => {
        res.json(format.user(user));
      })
      .catch(err => {
        res.send(err);
      });
    } else {
      res.status(403).send('Unauthorized');
    }
  })
  .catch(err => {
    res.status(err.status).send(err.message);
  });
});

router.post('/', (req, res, next) => {
  bcrypt.hash(req.body.password, BCRYPT_SALT_ROUNDS, (err, hash) => {
    if (err) res.send({ message: 'Could not encrypt password', status: 500 });

    handler.insertUser({
      firstName: req.body.name,
      lastName: '',
      email: req.body.email,
      password: hash,
      accountType: 'free',
      isAdmin: false,
      dateCreated: new Date(),
      preferences: {
        receiveNewsletter: false,
      }
    })
    .then (user => {
      const token = tokenUtil.generateToken(user);
      if (token) {
        let mappedUser = format.user(user);
        mappedUser.data.attributes.token = token;
        res.json(mappedUser);
      } else {
        res.send({ message: 'Invalid token', status: 403 });
      }
    })
    .catch(err => {
      res.status(err.status).send(err.message);
    });
  });
});

router.delete('/:id', (req, res, next) => {
  loginHandler.validate(req.headers['x-access-token'])
  .then(decoded => {
    if (decoded && decoded.isAdmin) {
      handler.deleteUser(req.params.id)
      .then(user => {
        res.json(format.user(user));
      })
      .catch(err => {
        res.send(err);
      });
    } else {
      res.status(403).send('Unauthorized');
    }
  })
  .catch(err => {
    res.status(err.status).send(err.message);
  });
});

module.exports = router;
