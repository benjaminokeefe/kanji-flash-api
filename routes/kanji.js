'use strict';

const
  express = require('express'),
  router = express.Router(),
  format = require('../utils/json-api-formatter'),
  handler = require('../handlers/kanji'),
  loginHandler = require('../handlers/login');

router.get('/', (req, res, next) => {
  let query = {};

  if (req.query) {
    Object.keys(req.query).forEach(key => {
      query[key] = req.query[key];
    });
  }

  handler.getKanji(query).then(kanji => {
    res.send(format.kanji(kanji));
  }, err => {
    res.send(err);
  });
});

router.get('/:id', (req, res, next) => {
  handler.getKanjiById(req.params.id).then(kanji => {
    res.send(format.kanji(kanji));
  }, err => {
    res.status(400).send('Invalid ID');
  });
});

router.post('/', (req, res, next) => {
  loginHandler.validate(req.headers['x-access-token'])
  .then(decoded => {
    if (decoded && decoded.isAdmin) {
      res.send(handler.insertKanji());
    } else {
      res.status(403).send('Unauthorized');
    }
  })
  .catch(err => {
    res.status(err.status).send(err.message);
  });
});

router.delete('/', (req, res, next) => {
  loginHandler.validate(req.headers['x-access-token'])
  .then(decoded => {
    if (decoded && decoded.isAdmin) {
      res.send(handler.deleteKanji());
    } else {
      res.status(403).send('Unauthorized');
    }
  })
  .catch(err => {
    res.status(err.status).send(err.message);
  });
});

module.exports = router;
