'use strict';

const
  express = require('express'),
  router = express.Router(),
  format = require('../utils/json-api-formatter'),
  handler = require('../handlers/blogs'),
  loginHandler = require('../handlers/login');

router.get('/', (req, res, next) => {
  handler.getBlogs()
  .then(blogs => {
    res.json(format.blog(blogs));
  })
  .catch(err => {
    res.send(err);
  });
});

router.get('/:id', (req, res, next) => {
  handler.getBlogById(req.params.id)
    .then(blog => {
      res.json(format.blog(blog));
    })
    .catch(err => {
      res.send(err);
    });
});

router.post('/query', (req, res, next) => {
  handler.getBlogsByQuery(req.body)
    .then(blogs => res.json(format.blog(blogs)))
    .catch(err => res.send(err));
});

router.post('/', (req, res, next) => {
  loginHandler.validate(req.headers['x-access-token'])
  .then(decoded => {
    if (decoded && decoded.isAdmin) {
      handler.insertBlog({
         dateCreated: req.body.data.attributes.dateCreated,
         title: req.body.data.attributes.title,
         author: req.body.data.attributes.author,
         entry: req.body.data.attributes.entry
      })
      .then (blog => {
        res.json(format.blog(blog));
      })
      .catch(err => {
        res.send(err);
      });
    } else {
      res.status(403).send('Unauthorized');
    }
  })
  .catch(err => {
    res.status(err.status).send(err.message)
  });
});

router.delete('/:id', (req, res, next) => {
  loginHandler.validate(req.headers['x-access-token'])
  .then(decoded => {
    if (decoded && decoded.isAdmin) {
      handler.deleteBlog(req.params.id)
      .then(blog => {
        res.json(format.blog(blog));
      })
      .catch(err => {
        res.send(err);
      });
    } else {
      res.status(403).send('Unauthorized');
    }
  })
  .catch(err => {
    res.status(err.status).send(err.message);
  });
});

module.exports = router;
