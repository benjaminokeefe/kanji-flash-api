const
    app = require('./app'),
    port = process.env.PORT || 5000;

app.listen(port, function() {
  console.log(`KANJI FLASH API is running on port ${port}`);
});