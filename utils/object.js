module.exports = {
  hasProperty (obj) {
    const args = [...arguments];

    for (let i = 1; i < args.length; i++) {
      if (!obj || !obj.hasOwnProperty(args[i])) {
        return false;
      }

      obj = obj[args[i]];
    }

    return true;
  }
}