const jwt = require('jsonwebtoken');

module.exports = {
  generateToken (user) {
    // _doc contains the actual user information in the mongoose model object
    return jwt.sign(user._doc, process.env.SECRET_ACCESS_KEY, {
      expiresIn: 2592000 // number of seconds in one month
    });
  }
};
