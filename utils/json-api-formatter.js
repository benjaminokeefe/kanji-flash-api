'use strict';

const objUtils = require('./object');

module.exports = {
    blog (blogs) {
      let mappedBlogs;

      if (blogs instanceof Array) {
        mappedBlogs = blogs.length ? blogs.map(b => mapBlog(b)) : [];
      } else {
        mappedBlogs = blogs ? mapBlog(blogs) : null;
      }

      return { "data": mappedBlogs };
    },

    user (users) {
      let mappedUsers;

      if (users instanceof Array) {
        mappedUsers = users.length ? users.map(u => mapUser(u)) : [];
      } else {
        mappedUsers = users ? mapUser(users) : null;
      }

      return { "data": mappedUsers };
    },

    kanji (kanji) {
      let mappedKanji;

      if (kanji instanceof Array) {
        mappedKanji = kanji.length ? kanji.map(k => mapKanji(k)) : [];
      } else {
        mappedKanji = kanji ? mapKanji(kanji) : null;
      }

      return { "data": mappedKanji };
    }
}

function mapBlog (blog) {
  return {
    "type": "blogs",
    "id": blog._id,
    "attributes": {
      dateCreated: blog.dateCreated,
      author: blog.author,
      title: blog.title,
      slug: blog.slug,
      entry: blog.entry
    }
  }
}

function mapUser (user) {
  return {
    "type": "user",
    "id": user._id,
    "attributes": {
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      accountType: user.accountType,
      isAdmin: user.isAdmin,
      dateCreated: user.dateCreated,
      receiveNewsletter: user.preferences.receiveNewsletter,
    }
  }
}

function mapKanji (kanji) {
  // The kanji argument is a Mongoose model, so the _doc property,
  // which contains a plain object containing the model's data, must be used
  // as the first argument to the _.has function.
  const doc = kanji._doc ? kanji._doc : kanji;
  let mappedKanji;
  
  try {
    mappedKanji = {
      "type": "kanji",
      "id": kanji._id,
      "attributes": {
        "writing": objUtils.hasProperty(doc, 'literal') ? doc.literal : '',
        "englishMeanings": objUtils.hasProperty(doc, 'meaning', 'english') ? doc.meaning.english : [],
        "frenchMeanings": objUtils.hasProperty(doc, 'meaning', 'french') ? doc.meaning.french : [],
        "spanishMeanings": objUtils.hasProperty(doc, 'meaning', 'spanish') ? doc.meaning.spanish : [],
        "portugeseMeanings": objUtils.hasProperty(doc, 'meaning', 'portugese') ? doc.meaning.portugese : [],
        "pinyinReadings": objUtils.hasProperty(doc, 'reading', 'pinyin') ? doc.reading.pinyin : [],
        "koreanRomanReadings": objUtils.hasProperty(doc, 'reading', 'korean_r') ? doc.reading.korean_r : [],
        "koreanHangulReadings": objUtils.hasProperty(doc, 'reading', 'korean_h') ? doc.reading.korean_h : [],
        "japaneseOnReadings": objUtils.hasProperty(doc, 'reading', 'ja_on') ? doc.reading.ja_on : [],
        "japaneseKunReadings": objUtils.hasProperty(doc, 'reading', 'ja_kun') ? doc.reading.ja_kun : [],
        "jlptLevel": objUtils.hasProperty(doc, 'jlpt', 'level') ? doc.jlpt.level : null,
        "jlptOrder": objUtils.hasProperty(doc, 'jlpt', 'order') ? doc.jlpt.order : null,
        "gradeLevel": objUtils.hasProperty(doc, 'grade') ? doc.grade : null 
      }
    }  
  } catch (e) {
    console.log(e);
  }

  return mappedKanji;
}
