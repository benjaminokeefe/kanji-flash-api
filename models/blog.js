'use strict';

const
  mongoose = require('mongoose'),
  Schema = mongoose.Schema;

let blogSchema = new Schema({
  dateCreated: Date,
  title: String,
  slug: String,
  author: String,
  entry: String
});

let Blog = mongoose.model('Blog', blogSchema);

module.exports = Blog;
