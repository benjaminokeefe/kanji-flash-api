'use strict';

const
  mongoose = require('mongoose'),
  Schema = mongoose.Schema;

let userSchema = new Schema({
  firstName: String,
  lastName: String,
  email: String,
  password: String,
  accountType: String,
  isAdmin: Boolean,
  dateCreated: Date,
  preferences: {
    receiveNewsletter: Boolean
  }
});

let User = mongoose.model('User', userSchema);

module.exports = User;
