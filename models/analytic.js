'use strict';

const
  mongoose = require('mongoose'),
  Schema = mongoose.Schema;

let analyticSchema = new Schema({
  userAgent: String,
  dateCreated: Date,
  latitude: Number,
  longitude: Number,
  action: String,
  userId: String
});

let Analytic = mongoose.model('Analytic', analyticSchema);

module.exports = Analytic;
