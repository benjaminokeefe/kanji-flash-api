'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let feedbackSchema = new Schema({
    type: String,
    text: String,
    dateCreated: Date,
    userId: Schema.ObjectId
}, { collection: 'feedback' });

let Feedback = mongoose.model('Feedback', feedbackSchema);

module.exports = Feedback;
