'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let deckSchema = new Schema({
  name: String,
  dateCreated: Date,
  ownerId: Schema.ObjectId,
  sharedUserIds: [Schema.ObjectId],
  kanji: [{ type: Schema.ObjectId, ref: 'Kanji' }]
});

let Deck = mongoose.model('Deck', deckSchema);

module.exports = Deck;
