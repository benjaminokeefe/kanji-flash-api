'use strict';

const
  mongoose = require('mongoose'),
  Schema = mongoose.Schema;

let kanjiSchema = new Schema({
  literal: String,
  meaning: {
    english: [String],
    french: [String],
    spanish: [String],
    portugese: [String]
  },
  reading: {
    pinyin: [String],
    korean_r: [String],
    korean_h: [String],
    ja_on: [String],
    ja_kun: [String]
  },
  jlpt: {
    level: Number,
    order: Number
  }
}, { collection: 'kanji-full' });

let Kanji = mongoose.model('Kanji', kanjiSchema);

module.exports = Kanji;
